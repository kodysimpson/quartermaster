package me.kodysimpson.quartermaster.menu.admin;

import me.kodysimpson.quartermaster.menu.Menu;
import me.kodysimpson.quartermaster.menu.PlayerMenuUtility;
import me.kodysimpson.quartermaster.utils.LockUtils;
import me.kodysimpson.quartermaster.utils.PlayerSettingsUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class PlayerSettingsAdminMenu extends Menu {

    @Override
    public String getMenuName() {
        return "QM Admin > Player Settings";
    }

    @Override
    public int getSlots() {
        return 27;
    }

    @Override
    public void handleMenu(InventoryClickEvent e, PlayerMenuUtility playerMenuUtility) {
        Player p = playerMenuUtility.getP();
        if (e.getCurrentItem().getType().equals(Material.BARRIER)) {
            new ManagePlayerAdminMenu().open(p);
        } else if (e.getCurrentItem().getType().equals(Material.GREEN_STAINED_GLASS_PANE)) {

            if (e.getSlot() == 9) { //Clicked on the first setting, Can make locks
                //Disable this setting
                PlayerSettingsUtils.setSetting("CanMakeLocks", false, playerMenuUtility.getUUIDToManage().toString());
                new PlayerSettingsAdminMenu().open(p);
                p.sendMessage(ChatColor.GREEN + "Altered lock settings for " + ChatColor.YELLOW + Bukkit.getOfflinePlayer(playerMenuUtility.getUUIDToManage()).getName());
            } else if (e.getSlot() == 13 && !LockUtils.unlimitedLocks()) {
                PlayerSettingsUtils.setSetting("UnlimitedLocks", false, playerMenuUtility.getUUIDToManage().toString());
                new PlayerSettingsAdminMenu().open(p);
                p.sendMessage(ChatColor.GREEN + "Altered lock settings for " + ChatColor.YELLOW + Bukkit.getOfflinePlayer(playerMenuUtility.getUUIDToManage()).getName());
            } else if (e.getSlot() == 17 && !PlayerSettingsUtils.hasUnlimitedFriends()) {
                PlayerSettingsUtils.setSetting("UnlimitedFriends", false, playerMenuUtility.getUUIDToManage().toString());
                new PlayerSettingsAdminMenu().open(p);
                p.sendMessage(ChatColor.GREEN + "Altered lock settings for " + ChatColor.YELLOW + Bukkit.getOfflinePlayer(playerMenuUtility.getUUIDToManage()).getName());
            } else if (e.getSlot() == 13 && LockUtils.unlimitedLocks()) {
                p.sendMessage(ChatColor.RED + "This cannot be altered since the setting is set to true in the config.yml");
            } else if (e.getSlot() == 17 && LockUtils.unlimitedLocks()) {
                p.sendMessage(ChatColor.RED + "This cannot be altered since the setting is set to true in the config.yml");
            }

        } else if (e.getCurrentItem().getType().equals(Material.RED_STAINED_GLASS_PANE)) {

            if (e.getSlot() == 9) { //Clicked on the first setting, Can make locks
                //Enable this setting
                PlayerSettingsUtils.setSetting("CanMakeLocks", true, playerMenuUtility.getUUIDToManage().toString());
                new PlayerSettingsAdminMenu().open(p);
                p.sendMessage(ChatColor.GREEN + "Altered lock settings for " + Bukkit.getOfflinePlayer(playerMenuUtility.getUUIDToManage()).getName());
            } else if (e.getSlot() == 13 && !LockUtils.unlimitedLocks()) {
                PlayerSettingsUtils.setSetting("UnlimitedLocks", true, playerMenuUtility.getUUIDToManage().toString());
                new PlayerSettingsAdminMenu().open(p);
                p.sendMessage(ChatColor.GREEN + "Altered lock settings for " + ChatColor.YELLOW + Bukkit.getOfflinePlayer(playerMenuUtility.getUUIDToManage()).getName());
            } else if (e.getSlot() == 17 && !PlayerSettingsUtils.hasUnlimitedFriends()) {
                PlayerSettingsUtils.setSetting("UnlimitedFriends", true, playerMenuUtility.getUUIDToManage().toString());
                new PlayerSettingsAdminMenu().open(p);
                p.sendMessage(ChatColor.GREEN + "Altered lock settings for " + ChatColor.YELLOW + Bukkit.getOfflinePlayer(playerMenuUtility.getUUIDToManage()).getName());
            } else if (e.getSlot() == 13 && LockUtils.unlimitedLocks()) {
                p.sendMessage(ChatColor.RED + "This cannot be altered since the setting is set to true in the config.yml");
            } else if (e.getSlot() == 17 && LockUtils.unlimitedLocks()) {
                p.sendMessage(ChatColor.RED + "This cannot be altered since the setting is set to true in the config.yml");
            }

        }
    }

    @Override
    public void setMenuItems(PlayerMenuUtility playerMenuUtility) {

        ArrayList<String> setting1_lore = new ArrayList<>();
        setting1_lore.add(ChatColor.LIGHT_PURPLE + "If enabled, the player can ");
        setting1_lore.add(ChatColor.LIGHT_PURPLE + "make locks. ");
        ItemStack setting1 = makeItem(Material.COMMAND_BLOCK, ChatColor.YELLOW + "" + ChatColor.BOLD + "Can Make Locks", setting1_lore);

        ArrayList<String> setting2_lore = new ArrayList<>();
        setting2_lore.add(ChatColor.LIGHT_PURPLE + "If enabled, the player can ");
        setting2_lore.add(ChatColor.LIGHT_PURPLE + "make as many locks as ");
        setting2_lore.add(ChatColor.LIGHT_PURPLE + "they want.");
        ItemStack setting2 = makeItem(Material.COMMAND_BLOCK, ChatColor.YELLOW + "" + ChatColor.BOLD + "Unlimited Locks", setting2_lore);

        ArrayList<String> setting3_lore = new ArrayList<>();
        setting3_lore.add(ChatColor.LIGHT_PURPLE + "If enabled, the player can ");
        setting3_lore.add(ChatColor.LIGHT_PURPLE + "give any amount of players ");
        setting3_lore.add(ChatColor.LIGHT_PURPLE + "access to their locks.");
        ItemStack setting3 = makeItem(Material.COMMAND_BLOCK, ChatColor.YELLOW + "" + ChatColor.BOLD + "Unlimited Friends", setting3_lore);

        ArrayList<String> clickToDisable = new ArrayList<>();
        clickToDisable.add(ChatColor.BLUE + "Click to Disable");

        ArrayList<String> clickToEnable = new ArrayList<>();
        clickToEnable.add(ChatColor.BLUE + "Click to Enable");

        ItemStack canMakeLocks;
        if (PlayerSettingsUtils.getSetting( "CanMakeLocks", playerMenuUtility.getUUIDToManage().toString())) {
            canMakeLocks = makeItem(Material.GREEN_STAINED_GLASS_PANE, ChatColor.GREEN + "Enabled", clickToDisable);
        } else {
            canMakeLocks = makeItem(Material.RED_STAINED_GLASS_PANE, ChatColor.RED + "Disabled", clickToEnable);
        }

        ItemStack unlimitedLocks;
        if (LockUtils.unlimitedLocks()) {
            ArrayList<String> lore1 = new ArrayList<>();
            lore1.add(ChatColor.AQUA + "Can only be disabled/enabled for ");
            lore1.add(ChatColor.AQUA + "player when disabled in config");
            unlimitedLocks = makeItem(Material.GREEN_STAINED_GLASS_PANE, ChatColor.GREEN + "Enabled", lore1);
        } else {
            if (PlayerSettingsUtils.getSetting("unlimitedLocks", playerMenuUtility.getUUIDToManage().toString())) {
                unlimitedLocks = makeItem(Material.GREEN_STAINED_GLASS_PANE, ChatColor.GREEN + "Enabled", clickToDisable);
            } else {
                unlimitedLocks = makeItem(Material.RED_STAINED_GLASS_PANE, ChatColor.RED + "Disabled", clickToEnable);
            }
        }

        ItemStack unlimitedFriends;
        if (PlayerSettingsUtils.hasUnlimitedFriends()) { //Config.yml says everyone has unlimited friends
            ArrayList<String> lore1 = new ArrayList<>();
            lore1.add(ChatColor.AQUA + "Can only be disabled/enabled for ");
            lore1.add(ChatColor.AQUA + "player when disabled in config");
            unlimitedFriends = makeItem(Material.GREEN_STAINED_GLASS_PANE, ChatColor.GREEN + "Enabled", lore1);
        } else {
            if (PlayerSettingsUtils.getSetting("unlimitedFriends", playerMenuUtility.getUUIDToManage().toString())) {
                unlimitedFriends = makeItem(Material.GREEN_STAINED_GLASS_PANE, ChatColor.GREEN + "Enabled", clickToEnable);
            } else {
                unlimitedFriends = makeItem(Material.RED_STAINED_GLASS_PANE, ChatColor.RED + "Disabled", clickToEnable);
            }
        }

        ItemStack back = makeItem(Material.BARRIER, ChatColor.RED + "Back");

        inventory.setItem(0, setting1);
        inventory.setItem(4, setting2);
        inventory.setItem(8, setting3);
        inventory.setItem(9, canMakeLocks);
        inventory.setItem(13, unlimitedLocks);
        inventory.setItem(17, unlimitedFriends);
        inventory.setItem(22, back);

        setFillerGlass();
    }
}
