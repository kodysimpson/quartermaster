package me.kodysimpson.quartermaster.menu.admin;

import me.kodysimpson.quartermaster.menu.Menu;
import me.kodysimpson.quartermaster.menu.PlayerMenuUtility;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class ManagePlayerAdminMenu extends Menu {

    @Override
    public String getMenuName() {
        return "QM Admin > Manage Player";
    }

    @Override
    public int getSlots() {
        return 9;
    }

    @Override
    public void handleMenu(InventoryClickEvent e, PlayerMenuUtility playerMenuUtility) {
        Player p = playerMenuUtility.getP();
        if (e.getCurrentItem().getType().equals(Material.REDSTONE_TORCH)) {
            new PlayerSettingsAdminMenu().open(p);
        } else if (e.getCurrentItem().getType().equals(Material.BARRIER)) {
            //send back to listings
            new PlayersWithLocksAdminMenu().open(p);
        } else if (e.getCurrentItem().getType().equals(Material.CHEST)) {
            new LocksListAdminMenu().open(p);
        }
    }

    @Override
    public void setMenuItems(PlayerMenuUtility playerMenuUtility) {

        ArrayList<String> locks_lore = new ArrayList<>();
        locks_lore.add(ChatColor.GREEN + "View and manage all locks");
        locks_lore.add(ChatColor.GREEN + " owned by the player.");
        ItemStack locks = makeItem(Material.CHEST, ChatColor.RED + "" + ChatColor.BOLD + "Locks", locks_lore);
        ArrayList<String> settings_lore = new ArrayList<>();
        settings_lore.add(ChatColor.GREEN + "View and or change the");
        settings_lore.add(ChatColor.GREEN + " settings for the player.");
        ItemStack settings = makeItem(Material.REDSTONE_TORCH, ChatColor.WHITE + "" + ChatColor.BOLD + "Settings", settings_lore);
        ItemStack back = makeItem(Material.BARRIER, ChatColor.RED + "Back");

        inventory.setItem(0, locks);
        inventory.setItem(4, settings);
        inventory.setItem(8, back);

        setFillerGlass();
    }
}
